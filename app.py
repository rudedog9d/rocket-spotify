import json
import os
from typing import List, Any, Iterator, Dict

import requests
import spotipy
from pydantic import BaseModel
from spotipy.oauth2 import SpotifyClientCredentials, SpotifyOAuth


# todo fetch playlist ID by name
g_playlist_id = os.getenv("SPOTIFY_PLAYLIST_ID")
sp_client = spotipy.Spotify(
    auth_manager=SpotifyClientCredentials(
        client_id=os.getenv("SPOTIFY_CLIENT_ID"),
        client_secret=os.getenv("SPOTIFY_CLIENT_SECRET")
    )
)
scopes = [
    'playlist-modify-public',
    'playlist-modify-private',
    'playlist-read-private',
    'playlist-read-collaborative',
    'app-remote-control',
    'streaming',
]
sp_oauth = spotipy.Spotify(
    auth_manager=SpotifyOAuth(
        scope=scopes,
        client_id=os.getenv("SPOTIFY_CLIENT_ID"),
        client_secret=os.getenv("SPOTIFY_CLIENT_SECRET"),
        redirect_uri=os.getenv("SPOTIFY_REDIRECT_URI")
    )
)


class Track(BaseModel):
    title: str
    artist: str
    album: str | None = None

    def __str__(self):
        ret = f"'{self.title}' By '{self.artist}'"
        if self.album:
            return ret + f" ({self.album})"
        return ret


def get_rocket_history(limit=100) -> List[Track]:
    base_url = "https://webapi.radioedit.iheart.com/graphql?operationName=GetCurrentlyPlayingSongs"
    variables = {
                     "slug":"wrkt-fm",
                     "paging": { "take": limit }
                 }
    extensions= {
        "persistedQuery": { "version":1, "sha256Hash":"386763c17145056713327cddec890cd9d4fea7558efc56d09b7cd4167eef6060"}
    }
    url = f"{base_url}&variables={json.dumps(variables)}&extensions={json.dumps(extensions)}"
    ret = []
    response = requests.get(url)
    for track in response.json()['data']['sites']['find']['stream']['amp']['currentlyPlaying']['tracks']:
        ret.append(Track(title=track['title'], artist=track['artist']['artistName'], album=track.get('albumName')))
    return ret


def search_track(title: str, artist: str, album: str = None) -> List[Dict[str, Any]]:
    """
    Search for a track and return the track URI
    :param title:
    :param artist:
    :param album:
    :return:
    """
    query = f"track:{title} artist:{artist}"
    if album:
        query += f" album:{album}"
    # results = sp.search(q=urllib.parse.quote(query), type='track', limit=20)
    results = sp_client.search(q=query, type='track', limit=20)
    if album and not len(results['tracks']['items']):
        print(f"failed to find track with album, trying without: {title} by {artist} ({album})")
        return search_track(title, artist)
    # for idx, track in enumerate(results['tracks']['items']):
    #     print(idx, track['name'], track['uri'])
    # todo handle no results
    # todo find better way to select track from list - `album == album|single`, `album != compilation`?
    return results['tracks']['items']


def get_all_playlist_items(playlist_id: str, limit=50):
    offset = 0
    data = sp_oauth.playlist_items(playlist_id, limit=limit)
    items = data['items']
    while data['next']:
        offset += limit
        data = sp_oauth.playlist_items(playlist_id, limit=limit, offset=offset)
        items.extend(data['items'])
    print(f"found {len(items)} items from playlist {playlist_id}")
    return items


def split_list(data: List[Any], chunk_size: int = 100) -> Iterator[List[Any]]:
    for i in range(0, len(data), chunk_size):
        yield data[i:i + chunk_size]


existing_tracks = get_all_playlist_items(g_playlist_id)
existing_track_uris = set([track['track']['uri'] for track in existing_tracks])

history = get_rocket_history(limit=100)

track_uris = []
for track in history:
    # uncomment this to print all tracks
    # print(track, end='\t')
    try:
        tracks = search_track(track.title, track.artist, track.album)
        if not len(tracks):
            print(f"failed to find track: {track}")
            continue
        track_uri = tracks[0]['uri']
        if track_uri not in existing_track_uris:
            track_uris.append(track_uri)
            # uncomment this to print only new tracks
            print(track, end='\t')
            print(f"(found {len(tracks)} results)")
    except Exception as e:
        print(f'failed to find track: {e}')

if not track_uris:
    print('no new tracks found')
    exit(0)

# todo support adding tracks to queue or playlist
# spotify limits playlist add to 100 tracks at a time
for chunk in split_list(track_uris, chunk_size=100):
    r = sp_oauth.playlist_add_items(g_playlist_id, chunk)
    print(r)
# todo deduplicate tracks already on playlist
print(f'{len(track_uris)} tracks added successfully')
